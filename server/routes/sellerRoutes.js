import express from 'express';

import {  
    authSeller,
    verifySeller,
    registerSeller
    } from '../controllers/sellerController.js';

const router = express.Router();

router.route('/').post(registerSeller);

router.route('/login').post(authSeller);

router.get('/verify', verifySeller);

export default router;