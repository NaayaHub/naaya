import express from "express";
import products from "./data/products.js";
import dotenv from 'dotenv';
import connectDB from "./config/db.js";

import productRoutes from './routes/productRoutes.js';
import userRoutes from './routes/userRoutes.js';
import orderRoutes from './routes/orderRoutes.js';
import sellerRoutes from './routes/sellerRoutes.js';
import { addItems, getFormEntries } from "./controllers/vendorFormController.js";


import { notFound, errorHandler } from "./middlewares/errorMiddleware.js";

dotenv.config();

connectDB();

const app = express();
app.use(express.json());

app.get('/', (req, res) => {
    res.send("API is running");
})

app.use('/api/products', productRoutes);
app.use('/api/users', userRoutes);
app.use('/api/orders', orderRoutes);
app.use('/api/seller', sellerRoutes);

// app.post("/api/create", addItems);
// app.get("/api/getForms", getFormEntries);

app.use(notFound);
app.use(errorHandler);

const PORT = process.env.PORT || 5000;

app.listen(PORT,
    console.log(`Server is running in ${process.env.NODE_ENV} mode on port ${PORT}`)
);