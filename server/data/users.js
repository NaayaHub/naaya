import bcrypt from 'bcryptjs';

const users = [
    {
        name: "Admin",
        email: "admin@example.com",
        password: bcrypt.hashSync("Admin@123", 10),
        isSuperAdmin: true,
        isAdmin: false,
        isVendor: false,
        isVerified: false,
    },
    {
        name: "John Doe",
        email: "johndoe@example.com",
        password: bcrypt.hashSync("John@123", 10),
        isSuperAdmin: false,
        isAdmin: true,
        isVendor: false,
        isVerified: false,
    },
    {
        name: "Jane Doe",
        email: "janedoe@example.com",
        password: bcrypt.hashSync("Admin@123", 10),
        isSuperAdmin: false,
        isAdmin: false,
        isVendor: true,
        isVerified: false,
    },
];

export default users;