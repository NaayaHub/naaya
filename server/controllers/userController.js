import asyncHandler from 'express-async-handler';
import Token from '../models/token.js';
import User from "../models/userModel.js";
import generateToken from '../utils/generateToken.js';
import crypto from 'crypto';
import sendEmail from '../utils/sendEmail.js';

/**
@desc Login User 
@router GET /api/users/login
@access public
 */

const authUser = asyncHandler(async (req, res) => {
    const { email, password } = req.body;

    const user = await User.findOne({ email });

    if (user && (await user.matchPassword(password))) {
        res.json({
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            isSuperAdmin: user.isSuperAdmin,
            isVendor: user.isVendor,
            isVerified: user.isVerified, 
            token: generateToken(user._id),
        });
    } else {
        res.status(401);
        throw new Error("Incorrect email or password");
    }
});

/**
@desc GET user profile
@router GET /api/users/profile
@access private
 */

const getUserProfile = asyncHandler(async (req, res) => {
    const user = await User.findById(req.user._id);

    if (user) {
        res.json({
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            isSuperAdmin: user.isSuperAdmin,
            isVendor: user.isVendor,
        });
    } else {
        res.status(404);
        throw new Error("User not found");
    }
});

/**
 * @desc	Register a new user
 * @router	POST /api/users/
 * @access	public
 */

const registerUser = asyncHandler( async (req, res) => {
    const { name, email, password } = req.body;

    const userExists = await User.findOne({ email });

    if (userExists) {
        res.status(400);
        throw new Error("User already exists");
    }

        const user = await User.create({
            name, email, password,
        });

        if (user) {
            res.status(201).json({
            _id: user._id,
			name: user.name,
			email: user.email,
			isAdmin: user.isAdmin,
            isSuperAdmin: user.isSuperAdmin,
            isVendor: user.isVendor,
            isVerified: user.isVerified,
			token: generateToken(user._id),
            });
                
            let token = await new Token({
                userId: user._id,
                token: crypto.randomBytes(64).toString("hex"),
            }).save();

            const message = `${process.env.BASE_URL}/users/verify?_id=${user._id}&token=${token.token}`;
            
            await sendEmail(user.email, "Verify email", message);
            
            res.send('Email send to your account please verify');
        }  else {
            res.status(400) //Bad request
            throw new Error('Invalid user data');
        }


} );

/**
 * @desc    Verify registered user
 * @router	GET /verify/:id/:token
 * @access	private
 */

const verifyUser = asyncHandler(async (req, res) => {
    try {
        const user = await User.findOne({ _id: req.query._id });
        if (!user) {
            return res.status(400).send("Invalid link");
        }

        const token = await Token.findOne({
            userId: user._id,
            token: req.query.token,
        });

        if(!token) {

            return res.status(400).send("Invalid link");
        } 

        await User.findOneAndUpdate({ _id: user._id }, { isVerified: true });
        await Token.findByIdAndRemove(token._id);
        res.status(200);
        res.send("Email is verified successfully");
    } catch (error) {
        throw new Error(error);
    }
});

/**
 * @desc		Update user profile
 * @router	PUT /api/users/profile
 * @access	private
 */

    const updateUserProfile = asyncHandler(async (req, res) => {
        const user = await User.findById(req.user._id);

        if (user) {
            user.name = req.body.name || user.name;
            user.email = req.body.email || user.email;
            if(req.body.password) {
                user.password = req.body.password;
            }

            const updatedUser = await user.save();

            res.json({
                _id: updatedUser._id,
                name: updatedUser.name,
                email: updatedUser.email,
                isAdmin: updatedUser.isAdmin,
                isSuperAdmin: updatedUser.isSuperAdmin,
                isVendor: updatedUser.isVendor,
                isVerified: updatedUser.isVerified,
                token: generateToken(updatedUser._id),
            })
        } else {  
            res.status(404);
            throw new Error("User not found");
        }
    });

    /**
 * @desc		Get all users
 * @router	GET /api/users/
 * @access	private/admin
 */

    const getUsers = asyncHandler(async (req, res) => {
        const users = await User.find({}).select('-password');
        res.json(users);
    });

    /**
 * @desc		Delete user
 * @router	DELETE /api/users/:id
 * @access	private/admin
 */

    const deleteUser = asyncHandler(async (req, res) => {
        const user = await User.findById(req.params.id);

        if (user) {
            await User.deletOne(user);
            res.json({ message: 'User deleted' });
        } else {
            res.status(404);
            throw new Error('User not found');
        }
    });

    /**
 * @desc		Get user by ID
 * @router	GET /api/users/:id
 * @access	private/admin
 */

    const getUserByID = asyncHandler(async (req, res) => {
        const user = await User.findById(req. params.id).select('-password');

        if (user) {
            res.json(user);
        } else {
            res.status(404);
            throw new Error('User not found');
        }
    });

  /**
 * @desc		Update a user
 * @router	PUT /api/users/:id
 * @access	private/admin
 */

  const updateUser = asyncHandler(async (req, res) => {
    const user = await User.findById(req.params.id);

    if(user) {
        user.name = req.body.name || user.name;
        user.email = req.body.email || user.email;
        user.isSuperAdmin = req.body.isSuperAdmin;
        user.isAdmin = req.body.isAdmin;
        user.isVendor = req.body.isVendor;

        const updateUser = await user.save();

        res.json({
            _id: updateUser._idname,
            name: updateUser.name,
            email: updateUser.email,
            isAdmin: updateUser.isAdmin,
            isSuperAdmin: updateUser.isSuperAdmin,
            isVendor: updateUser.isVendor,
        });
    };
  });

export { authUser, getUserProfile, registerUser, verifyUser, updateUserProfile, getUsers, deleteUser, getUserByID, updateUser };