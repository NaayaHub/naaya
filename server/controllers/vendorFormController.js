import asyncHandler from "express-async-handler";
import FormModel from "../models/vendorFormModel.js";

//@desc     Create new form entry
//@route    POST/api/create/
//@access   public

const addItems = asyncHandler(async (req, res) => {
  const {
    firstName, lastName, email, companyName, website, role, industryFocus, sell
  } = req.body;

    const entry = new FormModel({
      firstName, lastName, email, companyName, website, role, industryFocus, sell
    });

    const createEntry = await entry.save();
    res.status(201).json(createEntry);
  
});

// @desc        fetch all entries
// @route       GET /api/getForms
// @access      public
const getFormEntries = asyncHandler(async (req, res) => {
    const entries = await FormModel.find({});
    res.json(entries);
  });

export { addItems, getFormEntries };