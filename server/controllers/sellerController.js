import asyncHandler from 'express-async-handler';
import Seller from '../models/sellerModel.js';
import sendEmail from '../utils/sendEmail.js';
import generateToken from '../utils/generateToken.js';
import Token from '../models/token.js';

/**
 * @desc	Register a new seller
 * @router	POST /api/seller/
 * @access	public
 */

const registerSeller = asyncHandler( async (req, res) => {
    const { username, name, email, storeName, password, address, country, state, city, postalCode, contactNumber, gst } = req.body;

    const sellerExists = await Seller.findOne({ email });

    if(sellerExists) {
        res.status(400);
        throw new Error("Seller already exists!");
    }

    const seller = await Seller.create({
        name, username, email, storeName, password, address, country, state, city, postalCode, contactNumber, gst
    });

    if( seller ) {
        res.status(201).json({
            _id: seller._id,
            name: seller.name,
            username: seller.username,
            email: seller.email,
            address: seller.address,
            storeName: seller.storeName,
            country: seller.country,
            state: seller.state,
            city: seller.city,
            postalCode: seller.postalCode,
            contactNumber: seller.contactNumber,
            gst: seller.gst,
            isVerified: seller.isVerified,
            token: generateToken(seller._id),
        });

        let token = await new Token({
            sellerId: seller._id,
            token: crypto.randomBytes(64).toString("hex"),
        }).save();

        const message = `${process.env.BASE_URL}/seller/verify?_id=${seller._id}&token=${token.token}`;

        await sendEmail(seller.email, "Verify email", message);

        res.send("Email send to your account please verify within 1 hour");
    } else {
        res.status(400);
        throw new Error('Invalid user data')
    }
});

/**
 * @desc    Verify registered seller
 * @router	GET /verify/:id/:token
 * @access	private
 */

const verifySeller = asyncHandler( async (req, res) => {
    try {
        const seller = await Seller.findOne({ _id: req.query._id });
        if(!seller) {
            return res.status(400).send("Invalid link");
        }

        const token = await Token.findOne({
            sellerId: seller._id,
            token: req.query.token,
        });

        if(!token) {
            return res.status(400).send("Invalid link");
        }

        await Seller.findOneAndUpdate({ _id: seller._id }, { isVerified: true });
        await Token.findByIdAndRemove(token._id);
        res.status(200);
        res.send("Email is verified successfully")

    } catch (error) {
        throw new Error(error);
    }
} );


/**
@desc Login Seller 
@router GET /api/seller/login
@access public
 */

const authSeller = asyncHandler(async (req, res) => {
    const { email, password } = req.body;

    const seller = await Seller.findOne({ email });

    if(seller && (await seller.matchPassword(password))) {
        res.json({
            _id: seller._id,
            name: seller.name,
            email: seller.email,
            storeName: seller.storeName,
            isVerified: seller.isVerified,
            token: generateToken(seller._id),
        });
    } else {
        res.status(401);
        throw new Error("Incorrect email or password");
    }
});

export { authSeller, verifySeller, registerSeller };