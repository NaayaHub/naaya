import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';

const sellerSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true,
        },
        email: {
            type: String,
            required: true,
            unique: true,
            trim: true,
        },
        storeName: {
            type: String, 
            required: true,
            trim: true,
        },
        address: {
            type: String,
            required: true,
            trim: true,
        },
        country: {
            type: String,
            required: true,
        },
        state:{
            type: String,
            required: true,
        },
        postalCode:{
            type: Number,
            required: true,
        },
        gst:{
            type: String,
            required: true,
        },
        password: {
            type: String,
            required: true,
            trim: true,
        },
        isVerified: {
            type: Boolean,
            default: false,
        },
    }, 
    { timeStamps: true },
);

sellerSchema.method.matchPassword = async function (enteredPassword) {
    return await bcrypt.compare(enteredPassword, this.password);
};

sellerSchema.pre('save', async function (next) {
    if(!this.isModified("password")) {
        next();
    }

    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);
});

const Seller = mongoose.model('Seller', sellerSchema);

export default Seller;