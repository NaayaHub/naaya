import mongoose from "mongoose";

const formSchema = mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  companyName: {
    type: String,
    
  },
  website: {
    type: String,
    
  },
  role: {
    type: String,
    
  },
  choose: {
    type: String,
    
  },
  companyRevenue: {
    type: String,
    
  },
  dealSize: {
    type: Number
  },
  mainSkills: {
    type: String,
    
  },
  industryFocus: {
    type: String,
    
  },
  sell: {
    type: String,
    
  },
  whereIsSale: {
    type: String,
    
  },
  goodLead: {
    type: String,
    
  },
})

const FormModel = mongoose.model("Forms", formSchema);

export default FormModel;
