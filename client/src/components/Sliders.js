import { Image } from "@chakra-ui/react";
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";

const Sliders = ({ slides }) => {
  return (
    <Carousel infiniteLoop>
      {slides.map((slide) => {
        return <Image objectFit="cover" objectPosition="center" src={slide.image} height="29.5rem" w="inherit" />
      })}
    </Carousel>
  )
}

export default Sliders;