import { useState } from 'react';
import { Flex, Heading, Link, Box, Icon, Text, Button, Menu, MenuButton, MenuList, Image, useDisclosure, Popover, PopoverTrigger } from '@chakra-ui/react';
import {  HiOutlineMenuAlt3 } from 'react-icons/hi';
import { AiOutlineRight } from 'react-icons/ai';
import { IoIosMan } from 'react-icons/io';

const MenuItem = ({ children, url }) => {

  return (
    <Link
      href={url}
      fontSize="sm"
      letterSpacing="wide"
      color="white"
      mr="5"
      display="flex"
      alignItems="center"
      mt={{ base: 4, md: 0 }}>
      {children}
    </Link>
  );
};

const Navbar = () => {
  const [show, setShow] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Flex alignItems="start" direction="column" position="sticky">
      <Flex
      align="center"
      justify="space-between"  
      wrap="wrap"
      pt="4"
      pl="10"
      pr="3"
      w="100%"
      top="0"
      pos="relative"
      >
      <Box
        display={{ base: 'block', md: 'none', sm: 'block' }}
        onClick={() => setShow(!show)}>
        <Icon as={HiOutlineMenuAlt3} color="white" w="6" h="6" />
        <title>Menu</title>
      </Box>

      <Box
        display={{ base: show ? 'block' : 'none', md: 'flex' }}
        width={{ base: 'full', md: 'auto' }}
        alignItems="center">
        {/* <MenuItem url="/">
          Buyer Central
        </MenuItem> */}

        <MenuItem url="/">
            English-INR
        </MenuItem>

        <MenuItem url="/">
          <Icon as={IoIosMan} /> Sign In
        </MenuItem>

        <Button size="sm" borderRadius="20px" _active={{ _focus: false }} _hover={"disable: true"} textColor="#FFFFFF" bgColor="#ff6300">
          Sign Up
        </Button>

      </Box>
    </Flex>

    <Flex
      as="header"
      align="center"
      justify="space-between"
     
      wrap="wrap"
      py="6"
      // px="6"
      w="100%"
      top="0"
      pos="relative"
      zIndex="2">
      <Flex align="center" mr="5">
        <Text
          as="h1"
          color="whiteAlpha.800"
          size="md"
          
          letterSpacing="md">
           
            <Menu isLazy variant="unstyled" textColor="#FFFFF">
              <MenuButton  textColor="white">
                All Categories
              </MenuButton>
              <MenuList>
                <MenuItem minH="48px">
                <Image
                boxSize='2rem'
                borderRadius='full'
                src='https://placekitten.com/100/100'
                alt='Fluffybuns the destroyer'
                mr='12px'
                />
                <span style={{ color: "black" }}>Product 1</span>
                {/* <Icon as={<AiOutlineRight />} /> */}
                </MenuItem>

                <MenuItem minH="48px">
                <Image
                boxSize='2rem'
                borderRadius='full'
                src='https://placekitten.com/100/100'
                alt='Fluffybuns the destroyer'
                mr='12px'
                />
                <span style={{ color: "black" }}>Product 2</span>
                </MenuItem>
              </MenuList>
          </Menu>
          <Link textColor="#ffffff" ml="5" mr="5">Featured Selections</Link>
          <Link textColor="#ffffff" mr="5">Trade Assurance</Link>
        </Text>
      </Flex>

      <Box
        display={{ base: 'block', md: 'none', sm: 'block' }}
        onClick={() => setShow(!show)}>
        <Icon as={HiOutlineMenuAlt3} color="white" w="6" h="6" />
        <title>Menu</title>
      </Box>

      <Box
        display={{ base: show ? 'block' : 'none', md: 'flex' }}
        width={{ base: 'full', md: 'auto' }}
        alignItems="center">
        <MenuItem url="/">
          Buyer Central
        </MenuItem>

        <Popover trigger="hover" placement="bottom-start">
          <PopoverTrigger>
        <MenuItem url="/">
            Become a supplier
        </MenuItem>
        
        </PopoverTrigger>
        </Popover>

        <MenuItem url="/">
            Help Center
        </MenuItem>

        <MenuItem url="/"> 
            Get the app
        </MenuItem>

      </Box>
    </Flex>
    </Flex>
  );
};

export default Navbar;
