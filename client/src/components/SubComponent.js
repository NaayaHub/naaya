import React, {useState, useEffect} from "react";
import axios from "axios";

import { Flex,
     Box, 
     Breadcrumb,
      BreadcrumbItem, 
      BreadcrumbLink, 
      Input, 
      Button, 
      Image, 
      Icon, 
      SimpleGrid, 
      Card, 
      CardHeader, 
      Heading, 
      CardBody, 
      Text, 
      CardFooter, 
      Link,
      Stack,
      Grid,
      HStack
    } from "@chakra-ui/react";

import { TbMinusVertical } from 'react-icons/tb';
import { LiaShippingFastSolid } from 'react-icons/lia';
import LogoNaaya  from '../Image/naaya logo.png';
import { GiCargoShip, GiPlug, GiClothes, GiWashingMachine } from 'react-icons/gi';
import { SiWindows11 } from 'react-icons/si';
import { AiFillStar } from 'react-icons/ai';
import { IoPricetag } from 'react-icons/io5';

import Sliders from "./Sliders";
import { SlidesData } from "./SlideData";

import './SubComponent.css';

import  Nike  from '../Image/nj.jpg'
import Bottle from '../Image/bottle.jpg'

const SubComponent = () => {

  // const chngPos = () => {
  //   if (window.scrollY >= 10) {
  //     console.log(window.scrollY);
  //   } else {
      
      
  //   }
  // };

  // window.addEventListener("scroll", chngPos);

  const [products, setProducts] = useState([]);

//   useEffect(() => {
//     const fetchProducts = async () => {
//       const { data } = await axios.get('/api/products');
      
//       setProducts(data);
//     };

//     fetchProducts();
//   }, []);

    return (
        <>
        
        <Box w="auto" borderRadius="12px" fontWeight="bold" bgColor="white" display="flex" alignSelf="center" justifyContent="center" boxShadow="md">
        <Breadcrumb mx="4" my="1" textColor={"black"} separator={<TbMinusVertical size="3rem" style={{ marginLeft: "10px", marginRight: "10px" }} />}>
        <BreadcrumbItem isCurrentPage>
        <BreadcrumbLink fontSize="24px" fontWeight="400" textColor="black" href='#' style={{ display: "flex",alignItems: "center", color: "#614545", gap:"15px" }} ><Icon as={LiaShippingFastSolid} /> Ready to ship</BreadcrumbLink>
        </BreadcrumbItem>

        <BreadcrumbItem>
        <BreadcrumbLink fontSize="24px" fontWeight="400" textColor="black" href='#' style={{ display: "flex",alignItems: "center", color: "#614545", gap:"15px" }}><Icon as={GiCargoShip} /> Logistic services</BreadcrumbLink>
        </BreadcrumbItem>
        </Breadcrumb>
        </Box>
        
            <Flex alignItems="flex-start" gap="1.25rem" justifyContent="space-between" mt="4rem" direction="row" mb="2rem">

            
            <Flex w="17.063rem" alignItems="center" justifyContent="center" h="29.5rem" borderRadius="12px" px="1.25rem" bgColor="white" gap="1.2rem" direction="column" boxShadow="md" >
                <Heading mb="1.25rem" fontFamily={"Poppins"} fontWeight="700" textAlign="center" fontSize="24px">
                    Top Categories
                </Heading>
            <Flex color="#614545" fontWeight="500" gap={2} direction="column">
            <Link><Icon as={GiClothes} /> Consumer Electronics</Link>
            <Link><Icon as={GiClothes} /> Home & Garden</Link>
            <Link><Icon as={GiClothes} /> Beauty</Link>
            <Link><Icon as={GiClothes} /> Industrial Machinery</Link>
            <Link><Icon as={GiClothes} /> Entertainment</Link>
            <Link><Icon as={GiClothes} /> Apparel & Accessories</Link>
            <Link><Icon as={GiWashingMachine}/> Home Applications</Link>
            <Link><Icon as={GiPlug} /> Electrical Equipments</Link>
            <Link color={"#FF6300"}><Icon as={SiWindows11} /> View All Categories</Link>
            </Flex>
            </Flex>
           

            <Flex borderRadius="12px" w="60%"  color="white">
              <Sliders slides={SlidesData} />
            </Flex>

            <Flex  borderRadius="12px" p="1.25rem" bgColor="white" gap="1.2rem" direction="column" h="29.5rem" boxShadow="md" >
                {/* <Heading fontWeight="bold" textAlign="center" fontSize="xl">
                    Request for quotation
                </Heading> */}
            <Flex fontFamily="Poppins" fontWeight="bold" gap={2} direction="column">
            {/* <Link>Consumer Electronics</Link>
            <Link> Home & Garden</Link>
            <Link>Beauty</Link>
            <Link>Industrial Machinery</Link>
            <Link>Entertainment</Link>
            <Link><Icon as={GiClothes} /> Apparel & Accessories</Link>
            <Link><Icon as={GiWashingMachine}/> Home Applications</Link>
            <Link><Icon as={GiPlug} /> Electrical Equipments</Link>
            <Link><Icon as={SiWindows11} /> View All Categories</Link> */}
            <Heading fontFamily={"Poppins"} fontWeight="bold" textAlign="center" fontSize="xl">
                    Request for quotation
                </Heading>
            </Flex>
            </Flex>
            </Flex>

            <Flex gap="3" w="100%">
            {/* <Flex maxH="378px" maxW="467px" borderRadius="12px" direction="column" bgColor="white">  
            <Heading fontFamily={"Poppins"} textAlign="left" size='md' pb="2" pt="5" ml="1rem">Top Arrivals</Heading>
            <Flex px="3" gap="3" direction="column" pb="5">
            <Box display="flex" flexDirection="row" style={{ width:"197px", height:"192px" }}>
            
            <Image  objectFit="cover" objectPosition="center" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15);"
            src='https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80'
            alt='Green double couch with wooden legs'
            borderRadius='lg'
            />
            </Box>
            
            <Heading fontFamily={"Poppins"} size='md'>Living room Sofa</Heading>

            <Text color="#8D7F7F">Sofa for house</Text>
      
            <Text color='#ff6300' fontSize='2xl'>
                78% off
            </Text>
            </Flex>
            </Flex> */}

            <Box display="flex" flexDirection="column" justifyContent="center" borderRadius="12px" bgColor="white" w="467px" h="378px">
            <Heading  ml="1rem" fontFamily={"Poppins"} fontSize="24px" textAlign="left" size='md' pb="2" pt="5">Top Arrivals</Heading>
            <Flex justifyContent="center" gap="4"  direction="row">
            <Flex direction="column">
           
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src={Bottle} />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Water Bottle</Text>
                <Text fontSize="16px" color="#8D7F7F">Water bottle for kids</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
           
            </Flex>

            <Flex direction="column">
            <Flex  direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src={Nike} />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Jordans</Text>
                <Text fontSize="16px" color="#8D7F7F">Shoes for all sizes</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>
            </Flex>
            </Box>

            <Box display="flex" flexDirection="column" justifyContent="center" borderRadius="12px" bgColor="white" w="467px" h="378px">
            <Heading  ml="1rem" fontFamily={"Poppins"} fontSize="24px" textAlign="left" size='md' pb="2" pt="5">Top Ranking</Heading>
            <Flex justifyContent="center" gap="4"  direction="row">
            <Flex direction="column">
            <Flex   direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Living Room Sofa</Text>
                <Text fontSize="16px" color="#8D7F7F">Sofa</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>

            <Flex direction="column">
            <Flex  direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Living Room Sofa</Text>
                <Text fontSize="16px" color="#8D7F7F">Sofa</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>
            </Flex>
            </Box>

            <Box display="flex" flexDirection="column" justifyContent="center" borderRadius="12px" bgColor="white" w="467px" h="378px">
            <Heading ml="1rem" fontFamily={"Poppins"} fontSize="24px" textAlign="left" size='md' pb="2" pt="5">Savings Spotlight</Heading>
            <Flex justifyContent="center" gap="4"  direction="row">
            <Flex direction="column">
            <Flex   direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Living Room Sofa</Text>
                <Text fontSize="16px" color="#8D7F7F">Sofa</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>

            <Flex direction="column">
            <Flex  direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Living Room Sofa</Text>
                <Text fontSize="16px" color="#8D7F7F">Sofa</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>
            </Flex>
            </Box>

            {/* <Flex borderRadius="12px" direction="column" bgColor="white">  
            <Heading fontFamily={"Poppins"} textAlign="left" size='md' pb="2" pt="5" ml="1rem">Top Ranking</Heading>
            <Flex px="3" gap="3" direction="row">
            <Box maxW='15.75rem' maxH="15.75rem">
           
            <Image boxShadow="md"
            src='https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80'
            alt='Green double couch with wooden legs'
            borderRadius='lg'
            />
            <Heading fontFamily={"Poppins"} size='md'>Living room Sofa</Heading>
      
            <Text color='#ff6300' fontSize='2xl'>
                78% off
            </Text>
            
          </Box>
            
          <Box maxW='15.75rem' maxH="15.75rem">
           
           <Image boxShadow="md"
           src='https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80'
           alt='Green double couch with wooden legs'
           borderRadius='lg'
           />
           
           <Heading fontFamily={"Poppins"} size='md'>Living room Sofa</Heading>
     
           <Text color='#ff6300' fontSize='2xl'>
               78% off
           </Text>
         </Box>
    </Flex>

            </Flex> */}

            
            {/* <Flex borderRadius="12px" direction="column" bgColor="white">  
            <Heading fontFamily={"Poppins"} textAlign="left" size='md' pb="2" pt="5" ml="1rem">Saving Spotlight</Heading>
            <Flex px="3" gap="3" direction="row">
            <Box maxW='15.75rem' maxH="15.75rem">
           
            <Image boxShadow="md"
            src='https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80'
            alt='Green double couch with wooden legs'
            borderRadius='lg'
            />
            
            <Heading fontFamily={"Poppins"} size='md'>Living room Sofa</Heading>
      
            <Text color='#ff6300' fontSize='2xl'>
                78% off
            </Text>
          </Box>
            
          <Box maxW='15.75rem' maxH="15.75rem">
           
           <Image boxShadow="md"
           src='https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80'
           alt='Green double couch with wooden legs'
           borderRadius='lg'
           />
           <Heading fontFamily={"Poppins"} size='md'>Living room Sofa</Heading>
     
           <Text color='#ff6300' fontSize='2xl'>
               78% off
           </Text>
         </Box>
        </Flex>

            </Flex> */}
            </Flex>

            <Heading fontFamily={"Poppins"} fontWeight="500" color="#312222" my="4">Personal Protective Equipments</Heading>

            <Flex gap="3" w="100%">
            {/* <Flex maxH="378px" maxW="467px" borderRadius="12px" direction="column" bgColor="white">  
            <Heading fontFamily={"Poppins"} textAlign="left" size='md' pb="2" pt="5" ml="1rem">Top Arrivals</Heading>
            <Flex px="3" gap="3" direction="column" pb="5">
            <Box display="flex" flexDirection="row" style={{ width:"197px", height:"192px" }}>
            
            <Image  objectFit="cover" objectPosition="center" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15);"
            src='https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80'
            alt='Green double couch with wooden legs'
            borderRadius='lg'
            />
            </Box>
            
            <Heading fontFamily={"Poppins"} size='md'>Living room Sofa</Heading>

            <Text color="#8D7F7F">Sofa for house</Text>
      
            <Text color='#ff6300' fontSize='2xl'>
                78% off
            </Text>
            </Flex>
            </Flex> */}

            <Box display="flex" flexDirection="column" justifyContent="center" borderRadius="12px" bgColor="white" w="467px" h="378px">
            <Heading ml="1rem" fontFamily={"Poppins"} fontSize="24px" textAlign="left" size='md' pb="2" pt="5">Top Arrivals</Heading>
            <Flex justifyContent="center" gap="4"  direction="row">
            <Flex direction="column">
            <Flex   direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src="https://bit.ly/dan-abramov" alt="Product Image" />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Living Room Sofa</Text>
                <Text fontSize="16px" color="#8D7F7F">Sofa</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>

            <Flex direction="column">
            <Flex  direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src="https://s3-alpha-sig.figma.com/img/5e4b/c694/35f9d8f8e912ea563c4f6dba6effd5b4?Expires=1691971200&Signature=kdYG6OUSgAstvceXO3UCUB-DKraWpg7xYKcvKDfFDpN6H43tkn0CnOTLEBnT~9e-4KmEkFlLuwacFpK5pKkVgybQfaqb4VRFbDJxNzkCyCEd40iGEtErE1VBRqorAsIF0C866kuGuD8ZUDkBoLc2THvSB33N9Gtn6r~BqCainx9GNbRZSUj4biQTAiDmz-D6dtQrGejpNYLW4ElJ2EUmc51pfYgJMqQfMGT5x4KdZ9A-GKJHJWEhMXwlkCgmU6INcbJ~lsC7~eDxE46EApK2EcN53Av~tKUEEWYtoWrCqwF40d5BZbSdbUX3jQpr3wCFuM-9OShHDED001Y-Sw9Dvw__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4" />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Living Room Sofa</Text>
                <Text fontSize="16px" color="#8D7F7F">Sofa</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>
            </Flex>
            </Box>

            <Box display="flex" flexDirection="column" justifyContent="center" borderRadius="12px" bgColor="white" w="467px" h="378px">
            <Heading ml="1rem" fontFamily={"Poppins"} fontSize="24px" textAlign="left" size='md' pb="2" pt="5">Top Ranking</Heading>
            <Flex justifyContent="center" gap="4"  direction="row">
            <Flex direction="column">
            <Flex   direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Living Room Sofa</Text>
                <Text fontSize="16px" color="#8D7F7F">Sofa</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>

            <Flex direction="column">
            <Flex  direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Living Room Sofa</Text>
                <Text fontSize="16px" color="#8D7F7F">Sofa</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>
            </Flex>
            </Box>

            <Box display="flex" flexDirection="column" justifyContent="center" borderRadius="12px" bgColor="white" w="467px" h="378px">
            <Heading ml="1rem" fontFamily={"Poppins"} fontSize="24px" textAlign="left" size='md' pb="2" pt="5">Savings Spotlight</Heading>
            <Flex justifyContent="center" gap="4"  direction="row">
            <Flex direction="column">
            <Flex   direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Living Room Sofa</Text>
                <Text fontSize="16px" color="#8D7F7F">Sofa</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>

            <Flex direction="column">
            <Flex  direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="192px" w="197px" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            <Flex direction="column" justifyContent="space-between">
                <Text fontWeight="700" fontSize="20px" fontFamily={"Poppins"}>Living Room Sofa</Text>
                <Text fontSize="16px" color="#8D7F7F">Sofa</Text>
                <Text color='#ff6300' fontSize='2xl'>
                78% off
                </Text>
                </Flex>
            </Flex>
            </Flex>
            </Flex>
            </Box>
            </Flex>

            <Heading fontFamily={"Poppins"} fontWeight="500" color="#312222" my="4">Just for you</Heading>

           <Flex gap="4" direction="row">
            <Box borderRadius="12px"  display="flex" flexDirection="column" h="34.5rem" w="29.188rem" bgColor="white"> 
            <Image mx="auto" objectFit="cover" objectPosition="center" mt="2rem" px="1rem" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="21.5rem" w="22.063rem" src="https://s3-alpha-sig.figma.com/img/5e4b/c694/35f9d8f8e912ea563c4f6dba6effd5b4?Expires=1691971200&Signature=kdYG6OUSgAstvceXO3UCUB-DKraWpg7xYKcvKDfFDpN6H43tkn0CnOTLEBnT~9e-4KmEkFlLuwacFpK5pKkVgybQfaqb4VRFbDJxNzkCyCEd40iGEtErE1VBRqorAsIF0C866kuGuD8ZUDkBoLc2THvSB33N9Gtn6r~BqCainx9GNbRZSUj4biQTAiDmz-D6dtQrGejpNYLW4ElJ2EUmc51pfYgJMqQfMGT5x4KdZ9A-GKJHJWEhMXwlkCgmU6INcbJ~lsC7~eDxE46EApK2EcN53Av~tKUEEWYtoWrCqwF40d5BZbSdbUX3jQpr3wCFuM-9OShHDED001Y-Sw9Dvw__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4" />
            <Flex ml="1.25rem" direction="column" justifyContent="space-between">
            <Text  fontStyle="normal" lineHeight="normal" mt="1rem" fontSize="20px" fontWeight="400">Industrail Vaccum Cleaner</Text>
            <Flex alignItems="center" direction="row">
            {<AiFillStar style={{ color:"#E88B23" }} />}
            {<AiFillStar style={{ color:"#E88B23" }} />}
            {<AiFillStar style={{ color:"#E88B23" }} />}
            {<AiFillStar style={{ color:"#E88B23" }} />}
            <Text color="#8D7F7F" fontSize="16px">
                4509 Ratings & 490 Reviews
            </Text>
            </Flex>
            <Flex gap='2' direction="row" alignItems="center">
            <Text color="#E88B23" fontWeight="600" fontSize="20px">₹ 5,999</Text>
            <Text style={{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}}>₹9,999</Text>
            </Flex>
            {/* {<IoPricetag />} */}
            <HStack alignItems="center" gridGap="8px">
                <IoPricetag />  
                <Text fontWeight="500" fontSize="16px">Bank Offer</Text>
            </HStack>

            <Text fontWeight="400" fontSize="16px">Flat ₹1,250 off on HDFC Bank Credit Card EMI Transactionson orders priced between ₹15,000 to ₹39,999 <span style={{ color:"#E88B23" }}>T&C</span></Text>
            </Flex>
            </Box>

            <Box borderRadius="12px" display="flex" flexDirection="column" h="34.5rem" w="29.188rem" bgColor="white"> 
            <Image mx="auto" objectFit="cover" objectPosition="center" mt="2rem" px="1rem" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="21.5rem" w="22.063rem" src="https://s3-alpha-sig.figma.com/img/5e4b/c694/35f9d8f8e912ea563c4f6dba6effd5b4?Expires=1691971200&Signature=kdYG6OUSgAstvceXO3UCUB-DKraWpg7xYKcvKDfFDpN6H43tkn0CnOTLEBnT~9e-4KmEkFlLuwacFpK5pKkVgybQfaqb4VRFbDJxNzkCyCEd40iGEtErE1VBRqorAsIF0C866kuGuD8ZUDkBoLc2THvSB33N9Gtn6r~BqCainx9GNbRZSUj4biQTAiDmz-D6dtQrGejpNYLW4ElJ2EUmc51pfYgJMqQfMGT5x4KdZ9A-GKJHJWEhMXwlkCgmU6INcbJ~lsC7~eDxE46EApK2EcN53Av~tKUEEWYtoWrCqwF40d5BZbSdbUX3jQpr3wCFuM-9OShHDED001Y-Sw9Dvw__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4" />
            <Flex ml="1.25rem" direction="column" justifyContent="space-between">
            <Text  fontStyle="normal" lineHeight="normal" mt="1rem" fontSize="20px" fontWeight="400">Industrail Vaccum Cleaner</Text>
            <Flex alignItems="center" direction="row">
            {<AiFillStar style={{ color:"#E88B23" }} />}
            {<AiFillStar style={{ color:"#E88B23" }} />}
            {<AiFillStar style={{ color:"#E88B23" }} />}
            {<AiFillStar style={{ color:"#E88B23" }} />}
            <Text color="#8D7F7F" fontSize="16px">
                4509 Ratings & 490 Reviews
            </Text>
            </Flex>
            <Flex gap='2' direction="row" alignItems="center">
            <Text color="#E88B23" fontWeight="600" fontSize="20px">₹ 5,999</Text>
            <Text style={{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}}>₹9,999</Text>
            </Flex>
            {/* {<IoPricetag />} */}
            <HStack alignItems="center" gridGap="8px">
                <IoPricetag />  
                <Text fontWeight="500" fontSize="16px">Bank Offer</Text>
            </HStack>

            <Text fontWeight="400" fontSize="16px">Flat ₹1,250 off on HDFC Bank Credit Card EMI Transactionson orders priced between ₹15,000 to ₹39,999 <span style={{ color:"#E88B23" }}>T&C</span></Text>
            </Flex>
            </Box>

            <Box borderRadius="12px" display="flex" flexDirection="column" h="34.5rem" w="29.188rem" bgColor="white"> 
            <Image mx="auto" objectFit="cover" objectPosition="center" mt="2rem" px="1rem" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="21.5rem" w="22.063rem" src="https://s3-alpha-sig.figma.com/img/5e4b/c694/35f9d8f8e912ea563c4f6dba6effd5b4?Expires=1691971200&Signature=kdYG6OUSgAstvceXO3UCUB-DKraWpg7xYKcvKDfFDpN6H43tkn0CnOTLEBnT~9e-4KmEkFlLuwacFpK5pKkVgybQfaqb4VRFbDJxNzkCyCEd40iGEtErE1VBRqorAsIF0C866kuGuD8ZUDkBoLc2THvSB33N9Gtn6r~BqCainx9GNbRZSUj4biQTAiDmz-D6dtQrGejpNYLW4ElJ2EUmc51pfYgJMqQfMGT5x4KdZ9A-GKJHJWEhMXwlkCgmU6INcbJ~lsC7~eDxE46EApK2EcN53Av~tKUEEWYtoWrCqwF40d5BZbSdbUX3jQpr3wCFuM-9OShHDED001Y-Sw9Dvw__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4" />
            <Flex ml="1.25rem" direction="column" justifyContent="space-between">
            <Text  fontStyle="normal" lineHeight="normal" mt="1rem" fontSize="20px" fontWeight="400">Industrail Vaccum Cleaner</Text>
            <Flex alignItems="center" direction="row">
            {<AiFillStar style={{ color:"#E88B23" }} />}
            {<AiFillStar style={{ color:"#E88B23" }} />}
            {<AiFillStar style={{ color:"#E88B23" }} />}
            {<AiFillStar style={{ color:"#E88B23" }} />}
            <Text color="#8D7F7F" fontSize="16px">
                4509 Ratings & 490 Reviews
            </Text>
            </Flex>
            <Flex gap='2' direction="row" alignItems="center">
            <Text color="#E88B23" fontWeight="600" fontSize="20px">₹ 5,999</Text>
            <Text style={{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}}>₹9,999</Text>
            </Flex>
            {/* {<IoPricetag />} */}
            <HStack alignItems="center" gridGap="8px">
                <IoPricetag />  
                <Text fontWeight="500" fontSize="16px">Bank Offer</Text>
            </HStack>

            <Text fontWeight="400" fontSize="16px">Flat ₹1,250 off on HDFC Bank Credit Card EMI Transactionson orders priced between ₹15,000 to ₹39,999 <span style={{ color:"#E88B23" }}>T&C</span></Text>
            </Flex>
            </Box>
            </Flex>

            <HStack gap="4" alignItems="center" direction="row">
            <Heading fontFamily={"Poppins"} fontSize="36px" fontWeight="500" color="#312222" my="4">Trade services</Heading>
            <Text color="#8D7F7F">Naayatrade's trade services help insure that your purchased and protected.</Text>
            </HStack>

            <Flex mb="5rem" gap="4" direction="row">
            <Box display="flex" flexDirection="column" justifyContent="center" borderRadius="12px" bgColor="white" w="29.188rem" h="23.625rem">
            <Heading ml="2rem" fontFamily={"Poppins"} fontSize="24px" textAlign="left" size='md' pb="2" >Top Arrivals</Heading>
            <Flex justifyContent="center" gap="4"  direction="row">
            <Flex direction="column">
            <Flex mx="2rem" direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="12rem" w="26.313rem" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            
                <Text fontSize="16px" color="#8D7F7F">Trade assurance is as free order protection serviced offered by naayatrade.com</Text>
                </Flex>
    
            </Flex>
            </Flex>
            </Box>

            <Box display="flex" flexDirection="column" justifyContent="center" borderRadius="12px" bgColor="white" w="29.188rem" h="23.625rem">
            <Heading ml="2rem" fontFamily={"Poppins"} fontSize="24px" textAlign="left" size='md' pb="2" >Top Arrivals</Heading>
            <Flex justifyContent="center" gap="4"  direction="row">
            <Flex direction="column">
            <Flex mx="2rem" direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="12rem" w="26.313rem" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            
                <Text fontSize="16px" color="#8D7F7F">Trade assurance is as free order protection serviced offered by naayatrade.com</Text>
                </Flex>
    
            </Flex>
            </Flex>
            </Box>

            <Box display="flex" flexDirection="column" justifyContent="center" borderRadius="12px" bgColor="white" w="29.188rem" h="23.625rem">
            <Heading ml="2rem" fontFamily={"Poppins"} fontSize="24px" textAlign="left" size='md' pb="2">Top Arrivals</Heading>
            <Flex justifyContent="center" gap="4"  direction="row">
            <Flex direction="column">
            <Flex mx="2rem" direction="column">
            <Image boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="12rem" w="26.313rem" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
            
                <Text fontSize="16px" color="#8D7F7F">Trade assurance is as free order protection serviced offered by naayatrade.com</Text>
                </Flex>
    
            </Flex>
            </Flex>
            </Box>
            </Flex>
        </>
    )
}

export default SubComponent;