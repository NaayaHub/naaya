import { Flex, Icon, Heading, Text } from "@chakra-ui/react";
import { GrAppsRounded } from 'react-icons/gr';
import { MdAssuredWorkload } from 'react-icons/md';
import { AiFillCodeSandboxCircle, AiOutlineBlock, AiFillAppstore } from 'react-icons/ai';

const Homecards = () => {
  return (
    <>
    <Flex gap= "10" minW="80rem" justifyContent="center" paddingY="60px" px="0px" textColor="white">
      
        <Flex borderRadius="25px" direction="column" gap="3" padding="40px" boxShadow="12px" w="336px" h="338px" bgColor="hsla(0,0%,100%,.03)">
            <Icon bgColor="hsla(0,0%,100%,.03)" borderRadius="30px"  h="40px" w="40px" padding="5px" as={AiFillAppstore} />
            <Heading fontSize="1.5rem">Assured quality and transactions</Heading>
            <Text>Ensure production quality from verified suppliers, with your orders protected from payment to delivery.</Text>
        </Flex>
        <Flex borderRadius="25px" direction="column" gap="3" padding="40px" boxShadow="12px" w="336px" h="338px" bgColor="hsla(0,0%,100%,.03)">
        <Icon bgColor="hsla(0,0%,100%,.03)" borderRadius="30px"  h="40px" w="40px" padding="5px" as={MdAssuredWorkload} />
            <Heading fontSize="1.5rem">Assured quality and transactions</Heading>
            <Text>Ensure production quality from verified suppliers, with your orders protected from payment to delivery.</Text>
        </Flex>
        <Flex borderRadius="25px" direction="column" gap="3" padding="40px" boxShadow="12px" w="336px" h="338px" bgColor="hsla(0,0%,100%,.03)">
        <Icon bgColor="hsla(0,0%,100%,.03)" borderRadius="30px"  h="40px" w="40px" padding="5px" as={AiFillCodeSandboxCircle} />
            <Heading fontSize="1.5rem">One-stop trading solution</Heading>
            <Text>Order seamlessly from product/supplier search to order management, payment, and fulfillment.</Text>
        </Flex>
        <Flex borderRadius="25px" direction="column" gap="3" padding="40px" boxShadow="12px" w="336px" h="338px" bgColor="hsla(0,0%,100%,.03)">
        <Icon bgColor="hsla(0,0%,100%,.03)" borderRadius="30px"  h="40px" w="40px" padding="5px" as={AiOutlineBlock} />
            <Heading fontSize="1.5rem">Personalized trading experience</Heading>
            <Text>Get curated benefits, such as discounted samples and dedicated support, tailored to your business growth stage.</Text>
        </Flex>
    </Flex>
    </>
  )
};

export default Homecards;