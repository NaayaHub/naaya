import { useState } from 'react';
import { Flex, 
         Link, 
         Text, 
         Image, 
         Stack, 
         Button,
         Box, 
         Popover,
         Icon,
         IconButton,
         Collapse,
         PopoverTrigger,
         useDisclosure,
         PopoverContent,
         } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom'; 

 const NewNavbar = () => {

    const [navbar, setNavbar] = useState(false);
    const [display, setDisplay] = useState("none");

    const changeBackGround = () => {
        if(window.scrollY >= 80) {
            setNavbar(true)
        } else {
            setNavbar(false);
        }
    };

    window.addEventListener("scroll", changeBackGround);

  return (
    <>
        
    </>
  )
}

export default NewNavbar;