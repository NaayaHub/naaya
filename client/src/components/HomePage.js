import React, { useEffect, useState } from 'react';
import axios from 'axios';

import { Box, Flex, Input, Text, Button, InputGroup, InputRightElement, Heading } from '@chakra-ui/react';
import { AiOutlineSearch } from 'react-icons/ai'

import './HomePage.css'
import SubComponent from './SubComponent';


const Homepage = () => {

  return (
    <>
    <Flex height="83vh">
      {/* Left Side (Input Field) */}
      <Box width="50%" mt="10rem">
        
        <Text fontSize="xl" mb="25px" color="white" >Learn about <span style={{ color: "#ff6300", fontWeight: "bold" }}>naayatrade.com</span></Text>
        <Heading fontFamily={"Poppins"} mb="20px" as="h1" textColor="white" letterSpacing="1px">
            The leading B2B e-commerce platform for global trade
        </Heading>

        <InputGroup size="md">
        <Input
        
        _focus={{ bg:"white" }}
        _active={{ outline: "none" }}
        bg="white"
        borderRadius="25px"
          type="text"
          placeholder="Enter something here..."
          size="lg"
          variant="filled"
        />
        <InputRightElement>
        <Button _active={{ _focus: false }} _hover={"disable: true"} leftIcon={<AiOutlineSearch />}  bgColor="#FB6B02" textColor="white" mr= "4.5rem" mt="10px" minW="max-content" borderRadius="20vw" pl="10px" >Search</Button>
        </InputRightElement>
       </InputGroup>

       <Flex mt="5" alignItems="center">
        <Text textColor="#FFFFFF">Frequently searched:</Text>
        <Button _active={{ _focus: false }} borderRadius="20px" _hover={{ disable: true }} ml="2" textColor="#FFFFFF" variant={"outline"}>Sports</Button>
        <Button _active={{ _focus: false }} borderRadius="20px" _hover={{ disable: true }} ml="2" textColor="#FFFFFF" variant={"outline"}>iPhone 14 Pro Max</Button>
        <Button _active={{ _focus: false }} borderRadius="20px" _hover={{ disable: true }} ml="2" textColor="#FFFFFF" variant={"outline"}>Watch</Button>
      </Flex>
      
      </Box>

      {/* Right Side (Content) */}
      {/* <Box width="50%"> */}
        {/* Add your content for the right side here */}
      {/* </Box> */}

    </Flex>

    {/* <SubComponent /> */}
    </>
  );
};

export default Homepage;