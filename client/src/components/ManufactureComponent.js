import { Flex,
    Box, 
    Breadcrumb,
     BreadcrumbItem, 
     BreadcrumbLink, 
     Input, 
     Button, 
     Image, 
     Icon, 
     SimpleGrid, 
     Card, 
     CardHeader, 
     Heading, 
     CardBody, 
     Text, 
     CardFooter, 
     Link,
     Stack,
     Grid,
     HStack
   } from "@chakra-ui/react";

import { TbMinusVertical } from 'react-icons/tb';
import { LiaShippingFastSolid } from 'react-icons/lia';
import LogoNaaya  from '../Image/naaya logo.png';
import { GiCargoShip, GiPlug, GiClothes, GiWashingMachine } from 'react-icons/gi';
import { SiWindows11 } from 'react-icons/si';
import { FiArrowRight } from 'react-icons/fi';

import Sliders from "./Sliders";
import { SlidesData } from "./SlideData";

import './SubComponent.css';

import Bottle from '../Image/bottle.jpg'

const ManuFactureComponent = () => {

 // const chngPos = () => {
 //   if (window.scrollY >= 10) {
 //     console.log(window.scrollY);
 //   } else {
     
     
 //   }
 // };

 // window.addEventListener("scroll", chngPos);

 const data = ["Competitive OEM Factory", "Cooperated Suppliers", "Full Customization", "Registered Trademark"];

 const updateData = data.map((info) => {
    return <li>{info}</li>
 })

   return (
       <>
       <Box w="auto" borderRadius="12px" fontWeight="bold" bgColor="white" display="flex" alignSelf="center" justifyContent="center" boxShadow="md">
       <Breadcrumb mx="4" my="1" textColor={"black"} separator={<TbMinusVertical size="3rem" style={{ marginLeft: "10px", marginRight: "10px" }} />}>
       <BreadcrumbItem isCurrentPage>
       <BreadcrumbLink fontSize="24px" fontWeight="400" textColor="black" href='#' style={{ display: "flex",alignItems: "center", color: "#614545", gap:"15px" }} ><Icon as={LiaShippingFastSolid} /> Ready to ship</BreadcrumbLink>
       </BreadcrumbItem>

       <BreadcrumbItem>
       <BreadcrumbLink fontSize="24px" fontWeight="400" textColor="black" href='#' style={{ display: "flex",alignItems: "center", color: "#614545", gap:"15px" }}><Icon as={GiCargoShip} /> Logistic services</BreadcrumbLink>
       </BreadcrumbItem>

       {/* <BreadcrumbItem>
       <BreadcrumbLink fontSize="1.25rem" textColor="black" href='#' style={{ display: "flex",alignItems: "center", gap:"15px" }}><Icon as={GiCargoShip} /> Request for quotation</BreadcrumbLink>
       </BreadcrumbItem> */}
       </Breadcrumb>
       </Box>
       
           <Flex gap="1.25rem" justifyContent="space-between" mt="4rem" direction="row" mb="2rem">

           
           <Flex w="24.313rem" justifyContent="center" h="39rem" borderRadius="12px" px="1.25rem" bgColor="white" gap="1.2rem" direction="column" boxShadow="md" >
               <Heading mb="1.25rem" color="#614545" fontFamily={"Poppins"} fontWeight="500" textAlign="left" fontSize="24px">
                   Top Manufacturers
               </Heading>
           <Flex color="#614545" fontWeight="500" gap={2} direction="column">
           <Link fontWeight="400"><Icon as={GiClothes} /> Consumer Electronics</Link>
           <Link fontWeight="400"><Icon as={GiClothes} /> Home & Garden</Link>
           <Link fontWeight="400"><Icon as={GiClothes} /> Beauty</Link>
           <Link fontWeight="400"><Icon as={GiClothes} /> Industrial Machinery</Link>
           <Link fontWeight="400"><Icon as={GiClothes} /> Entertainment</Link>
           <Link fontWeight="400"><Icon as={GiClothes} /> Apparel & Accessories</Link>
           <Link fontWeight="400"><Icon as={GiWashingMachine}/> Home Applications</Link>
           <Link fontWeight="400"><Icon as={GiPlug} /> Electrical Equipments</Link>
           <Link fontWeight="400"><Icon as={GiClothes} /> Beauty</Link>
           <Link fontWeight="400"><Icon as={GiClothes} /> Industrial Machinery</Link>
           <Link fontWeight="400"><Icon as={GiClothes} /> Entertainment</Link>
           <Link fontWeight="400"><Icon as={GiClothes} /> Apparel & Accessories</Link>
           <Link fontWeight="400"><Icon as={GiWashingMachine}/> Home Applications</Link>
           <Link fontWeight="400"><Icon as={GiPlug} /> Electrical Equipments</Link>
           <Link fontWeight="400" color={"#FF6300"}><Icon as={SiWindows11} /> View All Categories</Link>
           </Flex>
           </Flex>
          

           <Flex w="42.5rem" h="39rem" borderRadius="12px"  bgColor="white">
           <Flex alignItems="center" mx="1rem" gap="4" direction="row">
                <Flex direction="column">
                    <Flex my="1rem" direction="column">
                    <Heading textAlign="center" color="#614545" fontSize="24px" fontFamily="Poppins" mb="1rem">Get Samples</Heading>
                    <Image mb="1rem" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="12rem" w="18.25rem" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
                    <Button size="sm" bgColor="#ff6300" textColor="white" borderRadius="12px">New Released</Button>
                    </Flex>
                    <Flex mb="1rem" direction="column">
                    <Image mb="1rem" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="12rem" w="18.25rem" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
                    <Button size="sm" bgColor="#ff6300" textColor="white" borderRadius="12px">New Released</Button>
                    </Flex>
                </Flex>

                <Flex direction="column">
                    <Flex my="1rem" direction="column">
                    <Heading textAlign="center" color="#614545" fontSize="24px" fontFamily="Poppins" mb="1rem">Factory live Q&A</Heading>
                    <Image mb="1rem" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="12rem" w="18.25rem" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
                    <Button size="sm" bgColor="#319820" textColor="white" borderRadius="12px">CEO Live</Button>
                    </Flex>
                    <Flex mb="1rem" direction="column">
                    <Image mb="1rem" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="12rem" w="18.25rem" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
                    <Button size="sm" bgColor="#319820" textColor="white" borderRadius="12px">Head Live</Button>
                    </Flex>
                </Flex>
           </Flex>
           
           </Flex>

           <Flex  w="24.313rem" h="39rem" borderRadius="12px" p="1.25rem" bgColor="white" gap="1.2rem" direction="column"  boxShadow="md" >
           <Heading textAlign="center" color="#614545" fontSize="24px" fontFamily="Poppins" mt="1rem" fontWeight="500" mb="1rem">Get Samples</Heading>
           <Flex fontFamily="poppins" fontWeight="bold" gap={2} direction="column">
           <Flex alignItems="center"  gap="4" direction="row">
                <Flex direction="column">
                    <Flex direction="column">
                    <Image mb="1rem" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="12rem" w="18.25rem" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
                    <Button size="sm" bgColor="#ff6300" textColor="white" borderRadius="12px">New Released</Button>
                    </Flex>
                    <Flex mb="1rem" direction="column">
                    <Image mb="1rem" boxShadow="2px 2px 6px 0px rgba(0, 0, 0, 0.15)" borderRadius="12px" h="12rem" w="18.25rem" src="https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80" />
                    <Button size="sm" bgColor="#ff6300" textColor="white" borderRadius="12px">New Released</Button>
                    </Flex>
                </Flex>
           </Flex>
           
           </Flex>
           </Flex>
           </Flex>

           <Flex justifyContent="left" mb="1rem" bgColor="white" borderRadius="12px">
            <Flex ml='1rem' alignItems="center" my="1rem" gap="9" direction="row">
            <Text color="#FF6300" fontSize="16px">All Categories</Text>
            <Icon as={FiArrowRight} />
            <Text fontSize="16px">Agriculture</Text>
            <Text fontSize="16px">Food & Beverages</Text>
            <Text fontSize="16px">Apparel & Accessories</Text>
            <Text fontSize="16px">Fabric & Textile raw materials</Text>
            <Text fontSize="16px">Home & Applications</Text>
            <Button bgColor="#FF6300" textColor="white">View All</Button>
            </Flex>
           </Flex>

           <Flex h="359px" gap="4" direction="row" mb="1rem" bgColor="white" borderRadius="12px">
        
            <Flex direction="column" ml='1rem' alignItems="left" my="1rem" >
            <Text fontWeight="500" color="#312222" fontSize="24px">Ayaan Domain Pvt Ltd</Text>
            <Flex gap="9" direction="row">
            <Text fontSize="16px" fontWeight="400">10 Yrs + 0777 Staff</Text>
            <Text fontSize="16px" fontWeight="400">US$ 7777+</Text>
            </Flex>
           
            <Text color="#FF6300">
                Rating & Reviews
            </Text>
            <Flex gap="2" direction="row">
            <Text color="#FF6300">
                4.9
            </Text>
            <Text color="#FF6300">
                7700+ reviews
            </Text>
            </Flex>

            <Text mt="1rem" fontWeight="500">Factory Capabilties</Text>
            <ul style={{ marginLeft: "1rem", fontSize:"16px" }}>{updateData}</ul>
            </Flex>

            <Flex mt="1rem" alignItems="center" direction="column">
            <Text mb="1rem" fontSize="16px">Image 1</Text>
            <Image borderRadius="12px" w="20.25rem" h="17.375rem" src={Bottle}/>
            </Flex>

            <Flex mt="1rem" alignItems="center" direction="column">
            <Text mb="1rem" fontSize="16px">Image 2</Text>
            <Image borderRadius="12px" w="20.25rem" h="17.375rem" src={Bottle}/>
            </Flex>

            <Flex mt="1rem" alignItems="center" direction="column">
            <Flex mb="1rem" direction="row" alignItems="center" gap="2">
            <Text fontSize="16px">Products</Text>
            <Icon as={ FiArrowRight } />
            </Flex>
            <Image borderRadius="12px" w="20.25rem" h="17.375rem" src={Bottle}/>
            </Flex>
           </Flex>

       </>
   )
}

export default ManuFactureComponent;