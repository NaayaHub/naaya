import {
    Flex,
    Link,
    Text,
    Image,
    Stack,
    Button,
    Box,
    Popover,
    Icon,
    IconButton,
    Collapse,
    PopoverTrigger,
    useDisclosure,
    PopoverContent,
  } from "@chakra-ui/react";
  import { Link as RouterLink } from "react-router-dom";
  import {
    HamburgerIcon,
    CloseIcon,
    ChevronDownIcon,
    ChevronRightIcon,
  } from "@chakra-ui/icons";
//   import tufflam from "../assets/tufflam logo.png";
  import { useState } from "react";
  
  const Navv = () => {
    // const { isOpen, onToggle } = useDisclosure();
    const [navbar, setNavbar] = useState(false);
    const [display, chngDisplay] = useState("none");
  
    const chngbg = () => {
      if (window.scrollY >= 80) {
        setNavbar(true);
      } else {
        setNavbar(false);
      }
    };
  
    window.addEventListener("scroll", chngbg);
  
    return (
      <>
        <Flex>
          <Flex
            bgColor={navbar ? "#fff" : "transparent"}
            zIndex="3"
            align="center"
            w="full"
            pos="fixed"
            top="0"
            boxShadow={navbar ? "10px 2px 25px #718096" : "none"}
            h={navbar ? "100px" : "80px"}
            transition="all .3s"
          >
            <Flex
              ml={2}
              // mt={8}
              align="center"
              flex={{ base: 1, md: "auto" }}
              display={{ base: "flex", md: "none" }}
            >
              <IconButton
                size="lg"
                bgColor="gray.400"
                icon={<HamburgerIcon />}
                onClick={() => chngDisplay("flex")}
              />
            </Flex>
            <Flex flex={{ base: 1 }} justify={{ base: "center", md: "start" }}>
              <Link
                _focus={{ outline: "none" }}
                as={RouterLink}
                _hover={{ color: "blackAlpha.700", textDecor: "none" }}
                to="/"
              >
                {/* <Image
                  w="100%"
                  src={tufflam}
                  pl={{ base: 0, lg: 20 }}
                  pr={{ base: 10, lg: 0 }}
                  h={navbar ? "80px" : "70px"}
                /> */}
              </Link>
            </Flex>
            <Flex display={{ base: "none", md: "flex" }} m={10}>
              <DeskNav />
            </Flex>
          </Flex>
  
          <Flex
            bgColor="#fff"
            w="100vw"
            zIndex={20}
            pos="fixed"
            top="0"
            left="0"
            overflowY={"auto"}
            flexDir={"column"}
            align="center"
            display={display}
          >
            <Flex
              mt={"1.8rem"}
              justify={"flex-start"}
              align="center"
              flex={{ base: 1, md: "auto" }}
              display={{ base: "flex", md: "none" }}
            >
              <IconButton
                bgColor="gray.400"
                size="lg"
                icon={<CloseIcon />}
                onClick={() => chngDisplay("none")}
              />
            </Flex>
            <MobileNav />
          </Flex>
        </Flex>
      </>
    );
  };
  
  const DeskNav = () => {
    return (
      <Stack mr={23} direction={"row"} spacing={4}>
        {NAV_ITEMS.map((navItem) => (
          <Popover trigger={"hover"} placement={"bottom-start"}>
            <PopoverTrigger>
              <Link
                p={2}
                href={navItem.href ?? "#"}
                fontSize="md"
                fontWeight={500}
                color="white"
                _focus={{ outline: "none" }}
                _hover={{
                  textDecor: "none",
                  color: "#0085db",
                }}
                transition={"all .8s ease"}
              >
                {navItem.label}
              </Link>
            </PopoverTrigger>
            {navItem.children && (
              <PopoverContent minW={{ base: "100%", lg: "1920px" }} bg="whiteAlpha.900">
                <Stack>
                  {navItem.children.map((child) => (
                    <DeskSubNav {...child} />
                  ))}
                </Stack>
              </PopoverContent>
            )}
          </Popover>
        ))}
      </Stack>
    );
  };
  
  const DeskSubNav = ({ label, href }) => {
    return (
      <Link
        href={href}
        role={"group"}
        display={"block"}
        p={2}
        _hover={{ textDecoration: "none" }}
        _focus={{ outline: "none" }}
      >
        <Stack direction={"row"} align="center">
          <Flex>
            <Text
              fontSize={"sm"}
              transition={"all .3s ease"}
              _groupHover={{ color: "#0085db" }}
              fontWeight={500}
              _hover={{ textDecoration: "none" }}
            >
              {label}
            </Text>
          </Flex>
          <Flex
            transition={"all .3s ease"}
            transform={"translateX(-10px)"}
            opacity={0}
            _groupHover={{ opacity: "100%", transform: "translateX(0)" }}
            justify={"flex-end"}
            align={"center"}
            flex={1}
          >
            <Icon color={"#0085db"} w={5} h={5} as={ChevronRightIcon} />
          </Flex>
        </Stack>
      </Link>
    );
  };
  
  const MobileNav = () => {
    return (
      <Stack w={{ md: "lg", sm: "sm" }} h="100vh" display={{ md: "none" }}>
        {NAV_ITEMS.map((navItem) => (
          <MobileNavItem {...navItem} />
        ))}
      </Stack>
    );
  };
  
  const MobileNavItem = ({ label, children, href }) => {
    const { isOpen, onToggle } = useDisclosure();
  
    return (
      <>
        <Stack display="block" maxW="100%" mt={"100px"} spacing={4}>
          <Stack direction="column" w="100%">
            <Box w="100%" justify={"space-between"} align="center">
              <Box as={Link} href={href ?? "#"}>
                <Text fontSize="lg" fontWeight={500} color="blackAlpha.900">
                  {label}
                </Text>
              </Box>
              {children && (
                <Box
                  as={Button}
                  onClick={children && onToggle}
                  variant="ghost"
                  size={"md"}
                  _focus={{ bgColor: "#fff" }}
                >
                  <Icon
                    as={ChevronDownIcon}
                    transition={"all .25s ease-in-out"}
                    transform={isOpen ? "rotate(180deg)" : ""}
                    w={6}
                    h={6}
                  />
                </Box>
              )}
            </Box>
          </Stack>
          <Collapse
            in={isOpen}
            animateOpacity
            style={{ marginTop: "0!important" }}
          >
            <Stack pl={4} align={"start"}>
              {children && children.map((child) => <MobSubNav {...child} />)}
            </Stack>
          </Collapse>
        </Stack>
      </>
    );
  };
  
  const MobSubNav = ({ href, label }) => {
    return (
      <Link
        href={href}
        role={"group"}
        display={"block"}
        p={1}
        _hover={{ textDecoration: "none" }}
        _focus={{ outline: "none" }}
      >
        <Stack direction={"row"} align="center">
          <Text
            transition={"all .3s ease"}
            fontWeight={500}
            _hover={{ textDecoration: "none" }}
            fontSize="md"
          >
            {label}
          </Text>
  
          <Flex
            transition={"all .3s ease"}
            transform={"translateX(-10px)"}
            opacity={0}
            _groupHover={{ opacity: "100%", transform: "translateX(0)" }}
            justify={"flex-end"}
            align={"center"}
            flex={1}
          >
            <Icon color={"#0085db"} w={5} h={5} as={ChevronRightIcon} />
          </Flex>
        </Stack>
      </Link>
    );
  };
  
  const NAV_ITEMS = [
    {
      label: "Products",
      href: "/products",
      children: [
        {
          label: "Prepreg",
          href: "/material/prepreg",
        },
        {
          label: "Glass Fabric Laminates",
          href: "/material/glassFabricLaminates",
        },
        {
          label: "Glass Mat Laminates",
          href: "/material/glassMatLaminates",
        },
        {
          label: "Phenolic Laminates",
          href: "/material/phenolicLaminates",
        },
  
        {
          label: "Mica Laminates",
          href: "/material/micaLaminates",
        },
        {
          label: "Tubes & Cylinders",
          href: "/material/tubesAndCylinders",
        },
        {
          label: "Pultrusion products & Structured profiles",
          href: "#",
        },
        {
          label: "Machined Components",
          href: "/material/machinedComponents",
        },
      ],
    },
    {
      label: "Industries",
      href: "/industries",
      children: [
        {
          label: "Aerospace",
          href: "/industry/aerospace",
        },
        {
          label: "Oil & Gas",
          href: "/industry/oilAndGas",
        },
        {
          label: "Transformers",
          href: "/industry/transformer",
        },
        {
          label: "Electric power",
          href: "/industry/electricPower",
        },
      ],
    },
    {
      label: "Customer Benefits",
      href: "/customerbenefits",
    },
    {
      label: "Careers",
      href: "/careers",
    },
    {
      label: "Contact",
      href: "/contact",
    },
  ];  
  
  export default Navv;