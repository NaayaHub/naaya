import React, { useState } from 'react';

import { Flex, Box, Heading, Text, Spacer, Icon, Button } from '@chakra-ui/react';

import { AiFillFacebook, AiFillLinkedin, AiFillYoutube, AiFillInstagram } from 'react-icons/ai'

const Footer = () => {
    
  return (
    <>
    <Flex justifyContent="center" gap="10rem" px="100px" direction="row" bgColor="#594F44">
        <Box mt="2rem" textColor="white">
            <Heading mb="1rem" fontSize="18px" fontWeight="500" fontFamily="Poppins">Trade Services</Heading>
            <Flex direction="column">
                <Text fontSize="12px">Help Centre</Text>
                <Text fontSize="12px">Report Abuse</Text>
                <Text fontSize="12px">Open a case</Text>
                <Text fontSize="12px">Policies & Rules</Text>
                <Text fontSize="12px">Get paid for your feedback</Text>
            </Flex>
        </Box>

        <Box mt="2rem" textColor="white">
            <Heading mb="1rem" fontSize="18px" fontWeight="500" fontFamily="Poppins">About Us</Heading>
            <Flex direction="column">
                <Text fontSize="12px">ABout Naayatrade</Text>
            </Flex>
        </Box>

        <Box mt="2rem" textColor="white">
            <Heading mb="1rem" fontSize="18px" fontWeight="500" fontFamily="Poppins">Trade Services</Heading>
            <Flex direction="column">
                <Text fontSize="12px">Trade assurance</Text>
                <Text fontSize="12px">Business identity</Text>
                <Text fontSize="12px">Logistics Services</Text>
                <Text fontSize="12px">Production Monitoring & Inspection services</Text>
                <Text fontSize="12px">Production Monitoring & Inspection services</Text>
            </Flex>
        </Box>

        <Box mt="2rem" textColor="white">
            <Heading mb="1rem" fontSize="18px" fontWeight="500" fontFamily="Poppins">Source</Heading>
            <Flex direction="column">
                <Text fontSize="12px">Resources</Text>
            </Flex>
        </Box>

        <Spacer h="3" />
    </Flex>

    <Flex gap="3" bgColor="#594F44" py="1rem" justifyContent="center" direction="row">
    <Text fontSize="12px">Follow us:</Text>
    <Icon as={ AiFillFacebook } />
    <Icon as={ AiFillInstagram } />
    <Icon as={ AiFillLinkedin } />
    <Icon as={ AiFillYoutube } />
    </Flex>
</>
  )
}

export default Footer;