import { useState } from 'react';
import { Flex, Heading, Link, Box, Icon, Text, Button } from '@chakra-ui/react';
import { HiOutlineMenuAlt3 } from 'react-icons/hi';
import { IoIosMan } from 'react-icons/io';

const MenuItem = ({ children, url }) => {

  return (
    <Link
      href={url}
      fontSize="sm"
      letterSpacing="wide"
      color="white"
      mr="5"
      display="flex"
      alignItems="center"
      mt={{ base: 4, md: 0 }}>
      {children}
    </Link>
  );
};

const TopComponent = () => {
  const [show, setShow] = useState(false);

  return (
    <Flex
      align="center"
      justify="space-between"  
      wrap="wrap"
      pt="4"
      pl="10"
      pr="3"
      w="100%"
      top="0"
      pos="relative"
      >
      <Flex align="center" mr="5">
        {/* <Text
          as="h1"
          color="whiteAlpha.800"
          size="md"
          letterSpacing="md">
          <Link textColor="#ffffff" mr="5"  _hover={{ color: 'gray.500', textDecor: 'none' }}>
           All Categories
          </Link>
          <Link textColor="#ffffff" mr="5">Featured Selections</Link>
          <Link textColor="#ffffff" mr="5">Trade Assurance</Link>
        </Text> */}
      </Flex>

      <Box
        display={{ base: 'block', md: 'none', sm: 'block' }}
        onClick={() => setShow(!show)}>
        <Icon as={HiOutlineMenuAlt3} color="white" w="6" h="6" />
        <title>Menu</title>
      </Box>

      <Box
        display={{ base: show ? 'block' : 'none', md: 'flex' }}
        width={{ base: 'full', md: 'auto' }}
        alignItems="center">
        {/* <MenuItem url="/">
          Buyer Central
        </MenuItem> */}

        <MenuItem url="/">
            English-INR
        </MenuItem>

        <MenuItem url="/">
          <Icon as={IoIosMan} /> Sign In
        </MenuItem>

        <Button size="sm" borderRadius="20px" _active={{ _focus: false }} _hover={"disable: true"} textColor="#FFFFFF" bgColor="#ff6300">
          Sign Up
        </Button>

      </Box>
    </Flex>
  );
};

export default TopComponent;
