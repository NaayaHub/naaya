import React from 'react'

export const MainLayout = ({children}) => {
  return (
    <div style={{ paddingLeft: "100px", paddingRight: "100px" }}>
        {children}
    </div>
  )
}

