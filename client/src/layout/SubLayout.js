import React from 'react'

export const SubLayout = ({children}) => {
  return (
    <div style={{ backgroundColor:"#51200B", paddingLeft: "100px", paddingRight: "100px" }}>
        {children}
    </div>

  )
}
