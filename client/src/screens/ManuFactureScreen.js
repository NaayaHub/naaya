import { Flex, Box } from "@chakra-ui/react";

import  { MainLayout }  from '../layout/MainLayout';
import { SubLayout }  from '../layout/SubLayout';


import TopComponent from '../components/TopComponent';
import SubComponent from '../components/SubComponent';
import Navbar from '../components/Navbar';
import Homepage from '../components/HomePage';

import backgroundImg from '../Image/bgImg.jpg';
import ManuFactureComponent from "../components/ManufactureComponent";

const ManuFactureScreen = () => {

   return (
     <>
    <Box backgroundImage={backgroundImg} backgroundSize="cover" backgroundRepeat="no-repeat">
    <MainLayout>
      <Navbar/>
      <Homepage />
    </MainLayout>
      </Box>
      
      <SubLayout>
      <ManuFactureComponent />
      </SubLayout>
      </>
   )
}

export default ManuFactureScreen;