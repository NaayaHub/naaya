import { useEffect, useState } from "react";

import Navbar from "../components/Navbar";
import Navv from "../components/Navv";

const Header = () => {
    const [showNavbar, setShowNavbar] = useState(true);

    const handleScroll = () => {
        const scrollTreshold = 1;
        const shouldShowNavbar = window.scrollY <= scrollTreshold;
        setShowNavbar(shouldShowNavbar);
    };

    useEffect(() => {
        window.addEventListener("scroll", handleScroll);

        return () => {
            window.removeEventListener("scroll", handleScroll);
        }
    }, []);

    return (
        <>
        {showNavbar ? <Navv /> : <p>This is new div</p>}
        </>
    )
}

export default Header;