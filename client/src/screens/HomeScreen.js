import React, { useEffect } from 'react'
import { Box } from '@chakra-ui/react';

import  { MainLayout }  from '../layout/MainLayout';
import { SubLayout }  from '../layout/SubLayout';
import TopComponent from '../components/TopComponent';
import SubComponent from '../components/SubComponent';
import Navbar from '../components/Navbar';
import Homepage from '../components/HomePage';

import backgroundImg from '../Image/bgImg.jpg';
import Header from './Header';
// import axios from 'axios';

     const HomeScreen = () => {

    //useEffect(  () => {
    // const newFunction = async () => {
    //   const reqData = (await axios.get("https://restcountries.com/v3.1/all")).data;
    //  const purified = reqData.map((ele) => console.log(ele.name.common));

    // }

    // newFunction();


  return (
    <>
    <Box backgroundImage={backgroundImg} backgroundSize="cover" backgroundRepeat="no-repeat">
    <MainLayout>
      <Header />
      <Homepage />
    </MainLayout>
      </Box>
      
      <SubLayout>
      <SubComponent />
      </SubLayout>
      </>
  );
};

export default HomeScreen;
