import { Button,
         Flex,
         FormControl,
         FormLabel,
         Heading,
         Input,
         Link,
         Spacer,
         Text,
         Textarea,
     } from "@chakra-ui/react"  

import { useState } from "react";

import  FormContainer  from "../components/FormContainer";

 const RfqForm = () => {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    
    const submitHandler = (e) => {
        e.preventDefault();
    }

  return (
    <Flex fontFamily="Poppins" w="full" alignItems="center" justifyContent="center" py="5">
        <FormContainer>
            <Heading fontFamily="Poppins" as="h1" mb="8" fontSize="3xl">
                What is RFQ?
            </Heading>

            <Text>
                Request for Quotation is a free and simple alternative for buyers looking for quality bids.
                 You will receive various quotations from qualified providers by sending out a simple request.
            </Text>

            <form onSubmit={submitHandler}>
            <FormControl id="firstName" isRequired>
                    <FormLabel htmlFor="name">First Name</FormLabel>
                    <Input 
                    id="firstName"
                    type="text"
                    placeholder="John"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    />
                </FormControl>

                <Spacer h="3" />

                <FormControl id="lastName" isRequired>
                    <FormLabel htmlFor="name">Last Name</FormLabel>
                    <Input 
                    id="lastName"
                    type="text"
                    placeholder="Doe"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    />
                </FormControl>

                <Spacer h="3" />

                <FormControl id="email" isRequired>
                    <FormLabel htmlFor="email">Email address</FormLabel>
                    <Input 
                    id="email"
                    type="email"
                    placeholder="johndoe@example.com"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    />
                </FormControl>

                <Spacer h="3" />

                <FormControl id="message" isRequired>
                    <FormLabel htmlFor="name">Tell us what you are looking for</FormLabel>
                    <Textarea 
                    id="message"
                    type="text"
                    placeholder="Please entry your query"
                    value={message}
                    onChange={(e) => setMessage(e.target.value)}
                    maxLength="400"
                    />
                </FormControl>

                <Spacer h="3" />

                <Button _active={{ _focus: false }} _hover={{ disable: true }} size="sm" bgColor="#ff6300" textColor="white" borderRadius="12px">Submit</Button>
            </form>
        </FormContainer>
    </Flex>
  )
}

export default RfqForm;