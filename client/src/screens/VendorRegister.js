import React, { useState } from 'react'

import { Button, Flex, FormControl, FormLabel, Input, useToast, Spacer, Text, Select, Heading } from '@chakra-ui/react';

import FormContainer from '../components/FormContainer';

import { Country, State, City } from 'country-state-city';

const VendorRegister = () => {

    const [username, setUserName] = useState("");
    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [storeName, setStoreName] = useState("");
    const [address, setAddress] = useState("");


  return (
    <>

    <Flex w="full" alignItems="center" justifyContent="center" py="5">
        <form>

        <Heading display="flex" justifyContent="center" as="h2">Vendor Registration</Heading>
            <FormContainer>

                {/* Username */}
                <FormControl id="username" isRequired>
                    <FormLabel>Username</FormLabel>
                    <Input
                     type='text'
                     maxLength={20}
                     placeholder='username'
                     value={username}
                     onChange={(e) => 
                        setUserName(e.target.value)
                     } 
                    />
                </FormControl>

                     <Spacer h="3" />

               {/* email */}
               <FormControl>
                     <FormLabel>Email</FormLabel>
                     <Input
                      type='email'
                      placeholder='johndoe@example.com'
                      value={email}
                      onChange={(e) => {
                        setEmail(e.target.value)
                      }}
                     />
               </FormControl>

               <Spacer h="3" />

                      {/* firstName */}
                      
                      <FormControl id="firstName" isRequired>
                    <FormLabel>First Name</FormLabel>
                    <Input
                     type='text'
                     maxLength={20}
                     placeholder='John'
                     value={firstName}
                     onChange={(e) => 
                        setFirstName(e.target.value)
                     } 
                    />
                </FormControl>

                     <Spacer h="3" />

                     {/* lastName */}

                     <FormControl id="lastName" isRequired>
                    <FormLabel>Last Name</FormLabel>
                    <Input
                     type='text'
                     maxLength={20}
                     placeholder='Doe'
                     value={lastName}
                     onChange={(e) => 
                        setLastName(e.target.value)
                     } 
                    />
                </FormControl>

                     <Spacer h="3" />

                     {/* store name */}

                    <FormControl id="storeName" isRequired>
                    <FormLabel>Store Name</FormLabel>
                    <Input
                     type='text'
                     maxLength={40}
                     placeholder='Your store name'
                     value={storeName}
                     onChange={(e) => 
                        setStoreName(e.target.value)
                     } 
                    />

                     <Text>
                        {`https://naayatrade.com/store/${storeName}`}
                    </Text>

                    {/* address */}
                </FormControl>

                     <Spacer h="3" />     

                     <FormControl id="address" isRequired>
                    <FormLabel>Address</FormLabel>
                    <Input
                     type='text'
                     maxLength={200}
                     placeholder='Address'
                     value={address}
                     onChange={(e) => 
                        setAddress(e.target.value)
                     } 
                    />
                </FormControl>

                <Spacer h="3" />

                <FormControl id="address" isRequired>
                    <FormLabel>Country</FormLabel>
                    
                </FormControl>

                <Spacer h="3" />

            </FormContainer>   
        </form>
    </Flex>
    </>
  )
}

export default VendorRegister;