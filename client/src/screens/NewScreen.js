import React, { useEffect } from 'react'
import { Box } from '@chakra-ui/react';

import  { MainLayout }  from '../layout/MainLayout';
import { SubLayout }  from '../layout/SubLayout';
import TopComponent from '../components/TopComponent';
import SubComponent from '../components/SubComponent';
import Navbar from '../components/Navbar';
import Homepage from '../components/HomePage';
import Homecards from '../components/Homecards';

import backgroundImg from '../Image/bgImg.jpg';
import Navv from '../components/Navv';
// import axios from 'axios';

     const HomeScreen = () => {

  return (
    <>
    <Box backgroundImage={backgroundImg} backgroundSize="cover" backgroundRepeat="no-repeat">
    <MainLayout>
      
      <Homepage />
    </MainLayout>
      </Box>
      
      <SubLayout>
      <Homecards />
      </SubLayout>
      </>
  );
};

export default HomeScreen;
