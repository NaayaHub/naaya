import { Box, Flex, Spacer } from '@chakra-ui/react';
import TopComponent from './components/TopComponent'
import Homepage from './components/HomePage';
import Navbar from './components/Navbar';
import backgroundImg from './Image/bgImg.jpg';
import SubComponent from './components/SubComponent';
import { MainLayout } from './layout/MainLayout';
import Navigation from './components/Navigation';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Button } from '@chakra-ui/react';
import { useState } from 'react';
import { SubLayout } from './layout/SubLayout';

import { extendTheme } from '@chakra-ui/react';


import './App.css'
import Footer from './components/Footer';
import ManuFactureScreen from './screens/ManuFactureScreen';
import HomeScreen from './screens/HomeScreen';
import  RfqForm  from './screens/RfqForm';
import VendorRegister from './screens/VendorRegister';
import SelectionDemo from './components/SelectionDemo';
import NewScreen from './screens/NewScreen';
import Navv from './components/Navv';

const  App = () => {

  return (
    // <>
    // <Router>    
    // <Flex as="main" direction="column" className='body'>
    // <Navv />
    // <Routes>
    //   <Route path="/" exact element={<NewScreen />} />
    //   <Route path="/manufacturers" element={<ManuFactureScreen />} />
    //   <Route path="/rfq" element={<RfqForm />} />
    //   <Route path="/vendorRegister" element={<VendorRegister />} />
    //   <Route path='/select' element={<SelectionDemo />} />
    //   </Routes>
    //   <Footer />
    // </Flex>
    // </Router>
    // </>

    <>
    <Navigation />

    </>
  );
} 

export default App;
