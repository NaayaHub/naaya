import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { ChakraProvider, Heading, extendTheme } from '@chakra-ui/react';


import '@fontsource/poppins/600.css';
import '@fontsource/poppins/400.css';

const root = ReactDOM.createRoot(document.getElementById('root'));

// const theme = extendTheme({
//   fonts : {
//     heading: `"Poppins", sans-serif`,
//     body: `"Poppins", sans-serif`,
//   },
// })

root.render(
    <React.StrictMode>
    <App />
    </React.StrictMode>
);
